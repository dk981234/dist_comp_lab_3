#include "banking.h"
#include "lamport_time.h"

timestamp_t time = 0;

timestamp_t get_lamport_time() { 
    ++time;
    return time > MAX_T ? MAX_T : time;
}

void compare_lamport_time(timestamp_t s_time) { 
    time = time > s_time ? time : s_time; }
