#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/wait.h>


#include "ipc.h"
#include "banking.h"
#include "pipes.h"
#include "extra_ipc.h"
#include "extra_banking.h"
#include "pa2345.h"
#include "logs.h"
#include "lamport_time.h"

local_id PROC_ID = PARENT_ID;

void receive_all_starts(int *read_pipes, local_id max_id){
    Message *msg = malloc(sizeof(Message));
  
    timestamp_t time;
    
    for(int i = 0; i <= max_id; i++){
        if(i == PARENT_ID || i == PROC_ID ) continue;
        receive(read_pipes, i, msg);
        compare_lamport_time(msg->s_header.s_local_time);
        if (msg->s_header.s_type == STARTED)
        {
            time = get_lamport_time();
        }
        else {
            i--;
        }
    }
    logging(log_received_all_started_fmt, time, PROC_ID); 
}



void start(int *read_pipes, int *write_pipes, local_id max_id, balance_t balance) {

    logging(log_started_fmt, 0, PROC_ID, getpid(), getppid(), balance);

    Message * msg = malloc(sizeof(Message));
    timestamp_t time = get_lamport_time();
    uint16_t size_of_paylaod = sprintf(msg->s_payload, log_started_fmt, time, PROC_ID, getpid(), getppid(), balance);
    init_message_header(msg, size_of_paylaod, STARTED, time);
    send_multicast(write_pipes, msg);
    receive_all_starts(read_pipes, max_id);

    free(msg);
}



void done(int *write_pipes, balance_t balance) {
    Message * msg = malloc(sizeof(Message));
    timestamp_t time = get_lamport_time();
    uint16_t size_of_paylaod = sprintf(msg->s_payload, log_done_fmt, time, PROC_ID, balance);
    init_message_header(msg, size_of_paylaod, DONE, time);

    send_multicast(write_pipes, msg);

    free(msg);
}

void stop(int *write_pipes) {
    Message * msg = malloc(sizeof(Message));
    init_message_header(msg, 0, STOP, get_lamport_time());

    send_multicast(write_pipes, msg);

    free(msg);
}

void K(local_id max_id, int **pipes) {

    receive_all_starts(pipes[0], max_id);

    bank_robbery(pipes, max_id);

    stop(pipes[1]);

    Message * msg = malloc(sizeof(Message));

    AllHistory * all = malloc(sizeof(AllHistory)); 
    all->s_history_len = 0;

    int done_counter = 0;

    while (done_counter < max_id || all->s_history_len < max_id) {
        receive_any(pipes[0], msg);
        compare_lamport_time(msg->s_header.s_local_time);
        timestamp_t time = get_lamport_time();
        switch (msg->s_header.s_type) {
            case BALANCE_HISTORY:
                    {
                    BalanceHistory * balanceHistory = malloc(sizeof(BalanceHistory));
                    memcpy(balanceHistory, msg->s_payload, msg->s_header.s_payload_len);
                    all->s_history[all->s_history_len++] = * balanceHistory;
                    free(balanceHistory);
                    break;
                    }
            case DONE:
                done_counter++;
                if(done_counter == max_id)
                    logging(log_received_all_done_fmt, time, PROC_ID);
                break;
            default:
                break;
        }
    }

    print_history(all);
    free(msg);
    free(all);
}

void C(balance_t balance, local_id max_id, int **pipes) {

    BalanceState *balanceState = malloc(sizeof(BalanceState));
    init_balance_state(balanceState, balance, 0, 0);

    BalanceHistory *balanceHistory = malloc(sizeof(BalanceHistory));
    init_balance_history(balanceHistory, balanceState, PROC_ID);

    start(pipes[0], pipes[1], max_id, balance);

    Message *msg = malloc(sizeof(Message));

    int stop = 0;
    int done_counter = 0;
    TransferOrder *transferOrder = malloc(sizeof(TransferOrder));

    while (!stop) {
        receive_any(pipes[0], msg);
        compare_lamport_time(msg->s_header.s_local_time);
        timestamp_t time = get_lamport_time();
        switch (msg->s_header.s_type) {
            case TRANSFER:
                if (msg->s_header.s_payload_len == sizeof(TransferOrder))
                {
                    memcpy(transferOrder, msg->s_payload, sizeof(TransferOrder));
                    if (transferOrder->s_src == PROC_ID) {
                        time = get_lamport_time();
                        init_balance_state(balanceState, (balance -= transferOrder->s_amount), time, transferOrder->s_amount);
                        update_balance_history(balanceHistory, balanceState);
                        msg->s_header.s_local_time = time;
                        send(pipes[1], transferOrder->s_dst, msg);
                        logging(log_transfer_out_fmt, msg->s_header.s_local_time,
                            transferOrder->s_src, transferOrder->s_amount, transferOrder->s_dst);
                    } 
                    else {
                        init_balance_state(balanceState, (balance += transferOrder->s_amount), time, 0);    
                        update_balance_history(balanceHistory, balanceState);
                        logging(log_transfer_in_fmt, time,
                                transferOrder->s_dst, transferOrder->s_amount, transferOrder->s_src);
                        init_message_header(msg, 0, ACK, get_lamport_time());
                        send(pipes[1], PARENT_ID, msg);
                    }
                }
                break;
            case STOP:
                done(pipes[1], balance);
                break;
            case DONE:
                if(++done_counter == max_id - 1) {
                    logging(log_received_all_done_fmt, time, PROC_ID); 
                    stop = 1;
                }
                break;
            default:
                break;
        }
    }

    timestamp_t time = get_lamport_time();

    init_balance_state(balanceState, balanceState->s_balance, time, 0);
    update_balance_history(balanceHistory, balanceState);
    int payload_len = sizeof(local_id) + sizeof(uint8_t)
        + sizeof(BalanceState) * balanceHistory->s_history_len;
    init_message_header(msg, payload_len, BALANCE_HISTORY, time);
    memcpy(msg->s_payload, balanceHistory, payload_len);
    send(pipes[1], PARENT_ID, msg);
    free(msg);
    free(balanceHistory);
    free(balanceState);
    free(transferOrder);
}


int main(int argc, char *argv[]) {
	if (argc >= 3 && strcmp(argv[1], "-p") == 0) {
        int n = atoi(argv[2]);
        if (n >= 2 && n <= 10 && n == argc - 3) {

            balance_t balance = -1;

            if (open_logfiles())
                exit(-1);
            init_pipes_buffer(n);

            for (local_id i = 1; i <= n; i++) {
                if (!fork()) {
                    PROC_ID = i;
                    balance = (balance_t) atoi(argv[2 + i]);
                    break;
                }
            }

            int **pipes = select_pipes(PROC_ID);
            close_pipes(n, PROC_ID);

            if (PROC_ID != PARENT_ID) {
                C(balance, (local_id) n, pipes);
            } else {
                K((local_id) n, pipes);
                while (wait(NULL) > 0);
            }
        }
    }

    return 0;
}
