#ifndef DIST_COMP_LAB_3_LAMPORT_TIME_H
#define DIST_COMP_LAB_3_LAMPORT_TIME_H

#include "banking.h"

void compare_lamport_time(timestamp_t s_time);

#endif
