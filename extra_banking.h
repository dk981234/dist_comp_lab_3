#ifndef DIST_COMP_LAB_2_EXTRA_BANKING_H
#define DIST_COMP_LAB_2_EXTRA_BANKING_H

#include "banking.h"

void update_balance_history(BalanceHistory * balanceHistory, BalanceState * new_balanceState);

void init_balance_history(BalanceHistory *balanceHistory, BalanceState *balanceState, local_id id);

void init_balance_state(BalanceState * balanceState, balance_t s_balance, timestamp_t s_time, balance_t s_balance_pending_in);

#endif
